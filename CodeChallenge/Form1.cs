﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeChallenge
{
    public partial class Form1 : Form
    {
        string LoadedText = String.Empty;
        int WordCount = 0; 
        bool started = false;
        Stopwatch timer;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.DefaultExt = ".txt";
            ofd.Filter = "Text documents (.txt)|*.txt";
            DialogResult = ofd.ShowDialog();
            if (DialogResult == DialogResult.OK)
            {
                try 
	            {	        
		            using(StreamReader sr = new StreamReader(ofd.FileName))
                    {
                        LoadedText = sr.ReadToEnd();
                        richTextBox1.Text = LoadedText;
                        WordCount = LoadedText.Split(" ".ToCharArray()).Count();
                    }
	            }
	            catch (Exception)
	            {
		            throw;
	            }
            }
        }

        private void btnStartStop_Click(object sender, EventArgs e)
        {
            if (this.started)
            {
                timer.Stop();
                if (richTextBox2.Text == LoadedText)
                {
                    double wpm = (this.WordCount / timer.Elapsed.TotalMinutes);
                    MessageBox.Show("Your wpm: " + wpm);
                }
                else
                {
                    MessageBox.Show("You are too retarded to type the correct text. You lose, and you have wasted: " + timer.Elapsed.TotalSeconds + " seconds.");
                }
                timer.Reset();
                btnStartStop.Text = "START";
                this.started = false;
            }
            else
            {
                timer = new Stopwatch();
                timer.Start();
                btnStartStop.Text = "STOP";
                this.started = true;

            }
        }
    }
}
